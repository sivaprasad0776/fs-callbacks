const faker = require('faker')
const fs = require("fs");
const path = require("path");
const dirPath = path.join(__dirname, "JSONDir");

function generateUsers(n) {

    let users = []

    for (let id = 1; id <= n; id++) {

        let firstName = faker.name.firstName();
        let lastName = faker.name.lastName();
        let email = faker.internet.email();

        users.push({
            "id": id,
            "first_name": firstName,
            "last_name": lastName,
            "email": email
        });
    }

    return { "users": users }
}

function generateProducts(n) {

    let products = []

    for (let id = 1; id <= n; id++) {

        let product = faker.commerce.product();
        let price = faker.commerce.price();
        let color = faker.commerce.color();

        products.push({
            "id": id,
            "product": product,
            "price": price,
            "color": color,
        });
    }

    return { "products": products }
}

function deleteDirectory(dirpath) {
    fs.rmdir(dirPath, err => {
        if (err) {
            console.log("Error occurred in deleting the directory", err.message);
        }
        else {
            console.log("Deleted the directory successfully")
        }
    })
}


function addUserData(dirpath, delDir) {
    console.log("ready to add files");

    const users = generateUsers(50);
    const usersJSON = JSON.stringify(users, null, " ");

    const usersPath = path.join(__dirname, "JSONDir/users.json");
    fs.writeFile(usersPath, usersJSON, (err) => {
        if (err) {
            console.log("Could not write the data into users file");
        }
        else {
            console.log("Successfully saved the users data");
            fs.unlink(usersPath, (err) => {
                if (err) {
                    console.log("could not delete users file");
                    console.log(err.message);
                }
                else {
                    console.log("successfully deleted users file");
                }
                addProductsFile(dirpath, delDir);
            });
        }
    })

}

function addProductsFile(dirpath, delDir) {

    const products = generateProducts(50);
    const productsJSON = JSON.stringify(products, null, " ");

    const productsPath = path.join(__dirname, "JSONDir/products.json");
    fs.writeFile(productsPath, productsJSON, (err) => {
        if (err) {
            console.log("Could not write the data into products file");
        }
        else {
            console.log("Successfully saved the products data");
            fs.unlink(productsPath, (err) => {
                if (err) {
                    console.log("could not delete products file");
                    console.log(err.message);
                }
                else {
                    console.log("successfully deleted products file");
                }
                delDir(dirPath);
            });
        }
    })

}


function addJSONData(){
    fs.access(dirPath, (error) => {
        if (error) {
            fs.mkdir(dirPath, function (error) {
                if (error) {
                    console.log("Error occurred while creating directory: ");
                    console.log(err.message);
                } else {
                    console.log("directory created");
                }
            });
        }
        else {
            console.log("Directory alreday exists:")
        }
        addUserData(dirPath,deleteDirectory );
    });
}

module.exports = addJSONData;